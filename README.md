RAPTR-SV Integration for Stampede
-----------------------
Eric T Dawson  
Texas Advanced Computing Center  
May 2015  

###What is it?  
  RAPTR-SV is a structural variant caller developed by Derek Bickhart at the USDA. It's available from https://github.com/njdbickhart/RAPTR-SV but a version is included in this repo. RAPTR-SV calls structural variants using paired-end reads. It is written in Java and has been threaded using Java's threading model. However, because of the queue runtime limits on TACC's Stampede supercomputer it still wasn't fast enough to finish a job.

  This repo aims to solve that issue using TACC's parametric launcher utilities. By dividing the run by chromosome and running multiple instances of RAPTR at once, this version allows completion of the cluster step on Stampede within the time limits of the normal queue. As the preprocess step is sufficiently fast to finish in the queue, we have simply left it in its vanilla form.

###How do I run it?  
This isn't really a way to run RAPTR on your own machine; see the original distribution of RAPTR for that.
You can also run the app from the iPlant Discovery Environment.


###What if I need help?  
Contact eric.t.dawson <AT> gmail.com or  
jcarson <AT> tacc.utexas.edu  
if you require assistance.