#!/bin/bash
#--------------------------------
# A script for running the graphlab Netflix simulation
# using multithreading (not OpenMPI)
#----------------------------------
#SBATCH -J raptr_cluster
#SBATCH -o raptrClus_run.o%j
#SBATCH -e raptrClus_run.o%j
#SBATCH -p gpu
#SBATCH -N 2
#SBATCH -n 32
#SBATCH -t 8:00:00
#SBATCH -A iPlant-Collabs

PATH=$PATH:"`pwd`/mrsfast"

module load jdk64


ref="../umd_3_1_Y_Mito.fa"
data="../AN.156.BP.02.aln.bam"
JAVA="`pwd`/java/jdk1.8.0_45/bin/java"

## Output prefix is super important.
## temp file folder is as well.

${JAVA} -jar RAPTR-SV.jar cluster -t 15 -p `pwd`/temp -d -s AN156.raptr.flat -o `pwd`/AN156.raptr.cluster -c Chr1
