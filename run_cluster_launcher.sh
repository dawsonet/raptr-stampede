#!/bin/bash

## MRSFAST must be on the system path to run RAPTR
PATH=$PATH:"`pwd`/mrsfast"

## A FASTA reference which has a corresponding MRSFAST index in the same directory
ref=${reference}

## The .flat file output by the preprocess step
flatfile=${flatfile}

## A basename for output
outname=${outname}

## The number of cores to use per task
cores=${coresPerProcess}

## Debugging flag
debug=${debug}

JAVA="`pwd`/java/jdk1.8.0_45/bin/java"
samtools="`pwd`/bin/samtools"

bamfile=`cat $flatfile | cut -f 1`

chroms=`${samtools} view -H ${bamfile} | grep "SQ" | grep -o "SN:[a-zA-Z0-9]*" | cut -d ":" -f 2`
if [ -e commandfile.txt ]
	then rm commandfile.txt
fi
rm -r outdir
mkdir outdir

if [[ "${debug}" == 0 ]]
then
    args=" -s ${flatfile} -t $cores -p `pwd`/temp/ "
else
    args=" -s ${flatfile} -t $cores -p `pwd`/temp/ -d"
fi

if [ ! -d "temp" ]
    then mkdir temp
fi


for i in $chroms
do
	echo "$JAVA -jar RAPTR-SV.jar cluster ${args} -o `pwd`/outdir/${outname}.raptr.cluster.${i} -c ${i}" >> commandfile.txt
done

python ~/launcher.py commandfile.txt $cores

## Concatenate all tandem, insertion, and sup files
## First, insertions
cd outdir

insertion_files=`ls | grep "insertions$" | sort -n`
cat ${insertion_files} > ${outname}.insertions.txt

## Next Tandems
tand_files=`ls | grep "tand$" | sort -n`
cat ${tand_files} > ${outname}.tandem.txt

## Finally tandem/insertion suppplementary files
insertion_sup_files=`ls | grep "insertion.sup$" sort -n`
cat ${insertion_sup_files} > ${outname}.insertion.sup.txt

tandem_sup_files=`ls | grep "tand.sup$" | sort -n`
cat ${tandem_sup_files} > ${outname}.tandem.sup.txt

mv ${outname}.insertions.txt ../
mv ${outname}.tandem.txt ../
mv ${outname}.insertion.sup.txt ../
mv ${outname}.tandem.sup.txt ../

## Remove pylauncher remnants and handle logs if needed
cd ../
if [[ "${debug}" == "1" ]]
then
    tar cvzf runtime_logs.tgz pylauncher*
    mkdir debugging
    mv RAPTR-SV.cluster debugging
    tar cvzf debug_logs.tgz debugging
    rm debugging
fi

rm -rf RAPTR-SV.cluster*
rm -rf pylauncher*
rm -rf temp
