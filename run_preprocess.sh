#!/bin/bash
#--------------------------------
# A script for running the graphlab Netflix simulation
# using multithreading (not OpenMPI)
#----------------------------------
#SBATCH -J raptr_cluster
#SBATCH -o raptrClus_run.o%j
#SBATCH -e raptrClus_run.o%j
#SBATCH -p gpu
#SBATCH -N 1
#SBATCH -n 16
#SBATCH -t 8:00:00
#SBATCH -A iPlant-Collabs

PATH=$PATH:"`pwd`/mrsfast"

module load jdk64


ref=${reference}
data=${data}
JAVA="`pwd`/java/jdk1.8.0_45/bin/java"

## Output prefix is super important.
## temp file folder is as well.
if [ ! -d "temp" ]
    then mkdir temp
fi

time ${JAVA} -jar RAPTR-SV.jar preprocess -i ${data} -o `pwd`/AN156.raptr -r ${ref} -d -t 12 -p `pwd`/temp/

rm -rf ./temp
