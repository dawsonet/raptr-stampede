#!/bin/bash
#--------------------------------
# A script to test the ability of MRSFAST to build
# an index given a fasta reference.
# Eric Dawson
# TACC, May 2015
#----------------------------------
#SBATCH -J mrsfast_index
#SBATCH -o mrsfast_run.o%j
#SBATCH -e mrsfast_run.o%j
#SBATCH -p gpu
#SBATCH -N 1
#SBATCH -n 16
#SBATCH -t 8:00:00
#SBATCH -A iPlant-Collabs

PATH=$PATH:"`pwd`/mrsfast"
ref="../umd_3_1_Y_Mito.fa"

mrsfast --index ${ref}
