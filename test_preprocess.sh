#!/bin/bash
#--------------------------------
# A script to test the RAPTR-SV preprocessing step
# Eric T Dawson
# TACC, May 2015
#----------------------------------
#SBATCH -J raptr_pre
#SBATCH -o raptrPre_run.o%j
#SBATCH -e raptrPre_run.o%j
#SBATCH -p normal
#SBATCH -N 1
#SBATCH -n 16
#SBATCH -t 8:00:00
#SBATCH -A iPlant-Collabs


PATH=$PATH:"`pwd`/mrsfast"

module load jdk64


ref="./umd_3_1_Y_Mito.fa"
data="./AN.156.BP.02.mem.bam"
JAVA="`pwd`/java/jdk1.8.0_45/bin/java"

## Output prefix is super important.
## temp file folder is as well.

if [ ! -d "temp" ]
    then mkdir temp
fi
time ${JAVA} -jar RAPTR-SV.jar preprocess -i ${data} -o `pwd`/AN156.raptr -r ${ref} -d -t 16 -p `pwd`/temp/

rm -rf temp
